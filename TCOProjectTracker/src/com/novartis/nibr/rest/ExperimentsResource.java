package com.novartis.nibr.rest;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.dbutils.DbUtils;
import org.glassfish.jersey.client.ClientConfig;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.novartis.nibr.rest.resource.Stackable;
import com.novartis.nibr.rest.resource.TCOProject;
import com.novartis.nibr.rest.resource.TCOProjectUpdate;
import com.novartis.nibr.rest.resource.ThreeColumnDescrResource;
import com.novartis.nibr.rest.resource.TwoColumnDescrResource;
import com.novartis.nibr.util.DBUtil;


@Path("api")
public class ExperimentsResource {

	private static final String DB_NAME = "tcoProjectTracker.db";
	private static final String EXP_ID_GETALL="all";
	private static final String PLACEHOLDER = "PLACEHOLDER";
			
	private static final String getProjectSQL = 
			"SELECT DISTINCT "
			+ "proj.id, "
			+ "proj.name, "
			+ "proj.code, "
			+ "proj.contacts, "
			+ "proj.description, "
			+ "proj.sponsor, "
			+ "proj.trafficlight, "
			+ "proj.objectives, "
			+ "proj.deliverables, "
			+ "proj.timelines, "
			+ "proj.wgstructure, "
			+ "t.descr as type, "
			+ "st.descr as sub_type, "
			+ "s.descr as status, "
			+ "pr.descr as priority, "
			+ "ph.descr as phase, "
			+ "pu.description as fup_description, "
			+ "pu.date as fup_date, "
			+ "pu.id as fup_id "
			+ "FROM projects proj "
			+ "LEFT OUTER JOIN type t on t.type_id = proj.type_id "
			+ "LEFT OUTER JOIN sub_type st on st.sub_type_id = proj.subtype_id "
			+ "LEFT OUTER JOIN status s on s.status_id = proj.status_id "
			+ "LEFT OUTER JOIN priority pr on pr.id = proj.priority_id "
			+ "LEFT OUTER JOIN phase ph on ph.id = proj.phase_id "
			+ "LEFT OUTER JOIN project_updates pu on pu.project_id = proj.id "
			+ "WHERECLAUSE";

	
//	START - TCO Project Tracker
	
	private JSONObject retrieveProjects(String sql){
				
		List<TCOProject> projectsList = new ArrayList<>();
		
		JSONObject jsonObject = new JSONObject();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		TCOProject proj = null;
		
		try {
			conn = DBUtil.getSQLiteConnection(DB_NAME);
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			int prevId = 0;
			boolean newProject;
			
			while ( rs.next() ) {
				int currentId = rs.getInt("id");
				
				if(prevId<currentId){
					proj = new TCOProject();
					
					proj.setId(rs.getInt("id"));
					proj.setName(rs.getString("name"));
					proj.setCode(rs.getString("code"));
					proj.setContacts(rs.getString("contacts"));
					proj.setDescription(rs.getString("description"));
					proj.setType(rs.getString("type"));
					proj.setSubType(rs.getString("sub_type"));
					proj.setStatus(rs.getString("status"));
					proj.setPriority(rs.getString("priority"));
					proj.setPhase(rs.getString("phase"));
					proj.setSponsor(rs.getString("sponsor"));
					proj.setTrafficlight(rs.getInt("trafficlight"));
					proj.setObjectives(rs.getString("objectives"));
					proj.setDeliverables(rs.getString("deliverables"));
					proj.setTimeline(rs.getString("timelines"));
					proj.setWgStructure(rs.getString("wgstructure"));
					
					rs.getInt("fup_id");
					
					if(!rs.wasNull()){
						List<TCOProjectUpdate> projUplist = new ArrayList<>();
						
						TCOProjectUpdate projUp = new TCOProjectUpdate();
						projUp.setDescription(rs.getString("fup_description"));
						projUp.setDate(rs.getString("fup_date"));
						projUp.setId(rs.getInt("fup_id"));
						
						projUplist.add(projUp);
						
						proj.setProjectupdates(projUplist);
					}
					
					prevId = currentId;
					newProject = true;
					
				}
				else{
					TCOProjectUpdate projUp = new TCOProjectUpdate();
					projUp.setDescription(rs.getString("fup_description"));
					projUp.setDate(rs.getString("fup_date"));
					projUp.setId(rs.getInt("fup_id"));
					
					proj.getProjectupdates().add(projUp);
					
					newProject = false;
				}
				
				if(newProject)
					projectsList.add(proj);
				
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				DbUtils.close(rs);
				DbUtils.close(stmt);
				DbUtils.close(conn);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		jsonObject.put("projects", projectsList);
		
		return jsonObject;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("projects")
	public Response getProjects() throws JSONException {
		JSONObject jsonObject = retrieveProjects(getProjectSQL.replace("WHERECLAUSE", ""));

		return Response.status(200).entity(jsonObject.toString()).build();
	}
	
	@GET
	@Path("projects/{projId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getProjetcById(@PathParam("projId")String projId){
		JSONObject jsonObject = retrieveProjects(getProjectSQL.replace("WHERECLAUSE", " WHERE proj.id = ".concat(projId)));
	
		return Response.status(200).entity(jsonObject.toString()).build();
	}
	
	
	@DELETE
	@Path("projects/{expId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response removeProject(@PathParam("expId")String projectId){
		JSONObject jsonObject = new JSONObject();
		
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		PreparedStatement ps2 = null;
		
		String deleteFup = "delete from project_updates where project_id = ?";
		String deleteProject = "delete from projects where id = ?";
		
		try {
			conn = DBUtil.getSQLiteConnection(DB_NAME);
			ps = conn.prepareStatement(deleteFup);
			ps2 = conn.prepareStatement(deleteProject);
			
			ps.setInt(1, Integer.valueOf(projectId));
			ps2.setInt(1, Integer.valueOf(projectId));
			
			ps.execute();
			ps2.execute();
			
			jsonObject.put("delete", "OK");
					
		}catch(SQLException e){
			e.printStackTrace();
			jsonObject.put("delete", "KO");
		}
		finally{
			try {
				DbUtils.close(rs);
				DbUtils.close(ps);
				DbUtils.close(ps2);
				DbUtils.close(conn);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return Response.status(200).entity(jsonObject.toString()).build(); 
	}
	
	@PUT
	@Path("projects/{expId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateProject(TCOProject proj){

		JSONObject responseJson = new JSONObject();
		ObjectMapper mapper = new ObjectMapper();
		
		String projects = this.getProjetcById(String.valueOf(proj.getId())).getEntity().toString();
		JSONObject JSONprojects = new JSONObject(projects);
		JSONArray JSONProjArray = new JSONArray(JSONprojects.get("projects").toString());
		
		Connection conn = null;
		PreparedStatement ps = null;
		
		try {
			@SuppressWarnings("unused")
			TCOProject oldProject = mapper.readValue(JSONProjArray.get(0).toString(),TCOProject.class);
			
			String updateProjectSQL = "UPDATE projects SET 	"
					+ "type_id = ?, "
					+ "subtype_id = ?, "
					+ "name = ?, "
					+ "code = ?, "
					+ "phase_id = ?, "
					+ "priority_id = ?, "
					+ "status_id = ?, "
					+ "contacts = ?, "
					+ "description = ?, "
					+ "sponsor = ?, "
					+ "trafficlight = ?, "
					+ "objectives = ?, "
					+ "deliverables = ?, "
					+ "timelines = ?, "
					+ "wgstructure = ? "
					+ "WHERE id = ?";
			
			String updateProjectupdatesSQL = "UPDATE project_updates SET "
					+ "description = ?, "
					+ "date = ? "
					+ "WHERE id = ?";
			
			String insertProjectupdatesSQL = "INSERT INTO project_updates (project_id, description, date) values (?, ?, ?)"; 
			String removeProjectupdatesSQL = "DELETE FROM project_updates WHERE id = ?"; 
			
			conn = DBUtil.getSQLiteConnection(DB_NAME);
			ps = conn.prepareStatement(updateProjectSQL);
			
//			type id
			if(proj.getType() != null){
				if(!proj.getType().equals(""))
					ps.setInt(1, getIdFromDescr("type", proj.getType()));
				else
					ps.setNull(1, Types.NULL);
			}
			else{
				ps.setNull(1, Types.NULL);
			}
//			sub type id
			if(proj.getSubType() != null){
				if(!proj.getSubType().equals(""))
					ps.setInt(2, getIdFromDescr("sub_type", proj.getSubType()));
				else
					ps.setNull(2, Types.NULL);
			}
			else{
				ps.setNull(2, Types.NULL);
			}
//			name
			if(proj.getName() != null){
				if(!proj.getName().equals(""))
					ps.setString(3, proj.getName());
				else
					ps.setNull(3, Types.NULL);
			}
			else{
				ps.setNull(3, Types.NULL);
			}
//			code
			if(proj.getCode() != null){
				if(!proj.getCode().equals(""))
					ps.setString(4, proj.getCode());
				else
					ps.setNull(4, Types.NULL);
			}
			else{
				ps.setNull(4, Types.NULL);
			}
//			phase id
			if(proj.getPhase()!= null){
				if(!proj.getPhase().equals(""))
					ps.setInt(5, getIdFromDescr("phase", proj.getPhase()));
				else
					ps.setNull(5, Types.NULL);
			}
			else{
				ps.setNull(5, Types.NULL);
			}
//			sub priority id
			if(proj.getPriority() != null){
				if(!proj.getPriority().equals(""))
					ps.setInt(6, getIdFromDescr("priority", proj.getPriority()));
				else
					ps.setNull(6, Types.NULL);
			}
			else{
				ps.setNull(6, Types.NULL);
			}
//			sub status id
			if(proj.getStatus() != null){
				if(!proj.getStatus().equals(""))
					ps.setInt(7, getIdFromDescr("status", proj.getStatus()));
				else
					ps.setNull(7, Types.NULL);
			}
			else{
				ps.setNull(7, Types.NULL);
			}
//			contacts
			if(proj.getContacts() != null){
				ps.setString(8, proj.getContacts());
			}
			else{
				ps.setString(8, "");
			}
//			description
			if(proj.getDescription() != null){
				ps.setString(9, proj.getDescription());
			}
			else{
				ps.setString(9, "");
			}
//			sponsor
			if(proj.getSponsor() != null){
				if(!proj.getSponsor().equals(""))
					ps.setString(10, proj.getSponsor());
				else
					ps.setNull(10, Types.NULL);
			}
			else{
				ps.setNull(10, Types.NULL);
			}
//			trafficlight
			if(proj.getTrafficlight() != null){
				if(!proj.getTrafficlight().equals(""))
					ps.setInt(11, proj.getTrafficlight());
				else
					ps.setNull(11, Types.NULL);
			}
			else{
				ps.setNull(11, Types.NULL);
			}
//			objectives
			if(proj.getObjectives() != null){
				if(!proj.getObjectives().equals(""))
					ps.setString(12, proj.getObjectives());
				else
					ps.setNull(12, Types.NULL);
			}
			else{
				ps.setNull(12, Types.NULL);
			}
//			deliverables
			if(proj.getDeliverables() != null){
				if(!proj.getDeliverables().equals(""))
					ps.setString(13, proj.getDeliverables());
				else
					ps.setNull(13, Types.NULL);
			}
			else{
				ps.setNull(13, Types.NULL);
			}
//			timelines
			if(proj.getTimeline() != null){
				if(!proj.getTimeline().equals(""))
					ps.setString(14, proj.getTimeline());
				else
					ps.setNull(14, Types.NULL);
			}
			else{
				ps.setNull(14, Types.NULL);
			}
//			wg structure
			if(proj.getWgStructure() != null){
				if(!proj.getWgStructure().equals(""))
					ps.setString(15, proj.getWgStructure());
				else
					ps.setNull(15, Types.NULL);
			}
			else{
				ps.setNull(15, Types.NULL);
			}
			
			ps.setInt(16, proj.getId());
			
			int update = ps.executeUpdate();
			
			List<Integer> pupIds = proj.getProjectupdates().stream().map(TCOProjectUpdate::getId).collect(Collectors.toList());
			List<Integer> oldPupIds = oldProject.getProjectupdates().stream().map(TCOProjectUpdate::getId).collect(Collectors.toList());
			
			List<TCOProjectUpdate> updatesToAdd = new ArrayList<>();
			List<TCOProjectUpdate> updatesToChange = new ArrayList<>();
			List<TCOProjectUpdate> updatesToRemove = new ArrayList<>();
			
			for(TCOProjectUpdate pup : proj.getProjectupdates()){	
				if(oldPupIds.contains(pup.getId())){
					updatesToChange.add(pup);
				}
				else{
					updatesToAdd.add(pup);
				}
			}
			
			for(TCOProjectUpdate oldPup : oldProject.getProjectupdates()){
				if(!pupIds.contains(oldPup.getId())){
					updatesToRemove.add(oldPup);
				}
			}
			
//			update project updates
			for(TCOProjectUpdate toChange : updatesToChange){
				ps = conn.prepareStatement(updateProjectupdatesSQL);
//				objectives
				if(toChange.getDescription() != null){
					if(!toChange.getDescription().equals("")){
						ps.setString(1, toChange.getDescription());
					}
					else{
						ps.setString(1, toChange.getDescription());
					}
				}
				else
					ps.setNull(1, Types.NULL);
//				date
				if(toChange.getDate() != null)
					ps.setString(2, toChange.getDate());
				else
					ps.setNull(2, Types.NULL);

				ps.setInt(3, toChange.getId());
				ps.executeUpdate();
			}
			
			
//			add project updates
			for(TCOProjectUpdate toAdd : updatesToAdd){
				ps = conn.prepareStatement(insertProjectupdatesSQL);
				ps.setInt(1, proj.getId());
//				objectives
				if(toAdd.getDescription() != null){
					if(!toAdd.getDescription().equals("")){
						ps.setString(2, toAdd.getDescription());
					}
					else{
						ps.setNull(2, Types.NULL);
					}
				}
				else
					ps.setNull(2, Types.NULL);
//				date
				if(toAdd.getDate() != null)
					ps.setString(3, toAdd.getDate());
				else
					ps.setNull(3, Types.NULL);
				
				ps.execute();
			}
			
//			remove project updates
			for(TCOProjectUpdate toRemove : updatesToRemove){
				ps = conn.prepareStatement(removeProjectupdatesSQL);
				ps.setInt(1, toRemove.getId());
				
				ps.execute();
				
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			try {
				DbUtils.close(ps);
				DbUtils.close(conn);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		responseJson.put("result", "OK");

		return Response.status(200).entity(responseJson.toString()).build();
	}
	
	private int getIdFromDescr(String tableName, String descr) {
		Integer returnedId = null; 
		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			String sql = "";
			if(tableName.equalsIgnoreCase("phase") || tableName.equalsIgnoreCase("priority"))
				sql ="select id from "+ tableName +" where descr = ?";
			else
				sql = "select "+tableName+"_id from "+ tableName +" where descr = ?";
			
			conn = DBUtil.getSQLiteConnection(DB_NAME);
			ps = conn.prepareStatement(sql);
			
			ps.setString(1, descr);
			
			rs = ps.executeQuery();
			
			if(rs.next())
				returnedId = rs.getInt(1);
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			try {
				DbUtils.close(rs);
				DbUtils.close(ps);
				DbUtils.close(conn);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return returnedId;
	}

	@POST
	@Path("projects")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createProject(TCOProject proj){

		JSONObject jsonObject = new JSONObject();
		
		Connection conn = DBUtil.getSQLiteConnection(DB_NAME);
		PreparedStatement ps = null;
		ResultSet rs = null;
		ResultSet rsKey = null;

		try {
			
			String seletTypeId = "select type_id from type where descr = ?";
			String seletSubTypeId = "select sub_type_id from sub_type where descr = ?";
			String seletPhaseId = "select id from phase where descr = ?";
			String seletPriorityId = "select id from priority where descr = ?";
			String seletSatusId = "select status_id from status where descr = ?";
			
			Integer typeId = null, subTypeId = null, phaseId = null, priorityId = null, statusId = null;

			String insertProj = "insert into projects (type_id, subtype_id, name, code, phase_id, priority_id, status_id, contacts, description, sponsor, trafficlight, objectives, deliverables, timelines, wgstructure) "
					+ "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			String insertFup = "insert into project_updates (project_id, description, date) values (?, ?, ?)";

			if(proj.getType() != null){
				ps = conn.prepareStatement(seletTypeId);
				ps.setString(1, proj.getType());
				rs = ps.executeQuery();
				if(rs.next()){
					typeId = rs.getInt(1); 
				}
				
			}
			
			if(proj.getSubType() != null){
				ps = conn.prepareStatement(seletSubTypeId);
				ps.setString(1, proj.getSubType());
				rs = ps.executeQuery();
				if(rs.next()){
					subTypeId = rs.getInt(1); 
				}
			}
			
			if(proj.getPhase() != null){
				ps = conn.prepareStatement(seletPhaseId);
				ps.setString(1, proj.getPhase());
				rs= ps.executeQuery();
				if(rs.next()){
					phaseId = rs.getInt(1); 
				}
			}
			
			if(proj.getPriority() != null){
				ps = conn.prepareStatement(seletPriorityId);
				ps.setString(1, proj.getPriority());
				rs= ps.executeQuery();
				if(rs.next()){
					priorityId = rs.getInt(1);
				}
			}
			
			if(proj.getStatus() != null){
				ps = conn.prepareStatement(seletSatusId);
				ps.setString(1, proj.getStatus());
				rs = ps.executeQuery();
				if(rs.next()){
					statusId = rs.getInt(1); 
				}
			}
			
			ps = conn.prepareStatement(insertProj);
			
			if(typeId != null)
				ps.setInt(1, typeId);
			else
				ps.setNull(1, Types.NULL);
			if(subTypeId != null)
				ps.setInt(2, subTypeId);
			else
				ps.setNull(2, Types.NULL);
			if(proj.getName() != null)
				ps.setString(3, proj.getName());
			else
				ps.setNull(3, Types.NULL);
			if(proj.getCode() != null)
				ps.setString(4, proj.getCode());
			else
				ps.setNull(4, Types.NULL);
			if(phaseId != null)
				ps.setInt(5,  phaseId);
			else
				ps.setNull(5, Types.NULL);
			if(priorityId != null)
				ps.setInt(6, priorityId);
			else
				ps.setNull(6, Types.NULL);
			if(statusId != null)
				ps.setInt(7, statusId);
			else
				ps.setNull(7, Types.NULL);
			if(proj.getContacts() != null)
				ps.setString(8, proj.getContacts());
			else
				ps.setString(8, "");
			if(proj.getDescription() != null)
				ps.setString(9, proj.getDescription());
			else
				ps.setNull(9, Types.NULL);
			if(proj.getSponsor() != null)
				ps.setString(10, proj.getSponsor());
			else
				ps.setString(11, "");
			if(proj.getTrafficlight() != null)
				ps.setInt(11, proj.getTrafficlight());
			else
				ps.setString(11, "");
			if(proj.getObjectives() != null){
				ps.setString(12,  proj.getObjectives());
			}
			else
				ps.setNull(12, Types.NULL);
			if(proj.getDeliverables() != null){
				ps.setString(13,  proj.getDeliverables());
			}
			else
				ps.setNull(13, Types.NULL);
			if(proj.getTimeline() != null){
				ps.setString(14,  proj.getTimeline());
			}
			else
				ps.setNull(14, Types.NULL);
			if(proj.getWgStructure() != null){
				ps.setString(15,  proj.getWgStructure());
			}
			else
				ps.setNull(15, Types.NULL);
			
			
			ps.execute();
			rsKey= ps.getGeneratedKeys();
			if(rsKey.next()){
				proj.setId(rsKey.getInt(1));
			}
			
			for(TCOProjectUpdate fup : proj.getProjectupdates()){
				
				ps = conn.prepareStatement(insertFup);
				ps.setInt(1, proj.getId());
				if(fup.getDescription() != null ){
					if(!fup.getDescription().equals("")){
						ps.setString(2, fup.getDescription());
					}
					else
						ps.setNull(2, Types.NULL);
				}
				else
					ps.setNull(2, Types.NULL);
				
				ps.setString(3, fup.getDate());
				
				ps.execute();
			}
			jsonObject.put("result", "OK");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			jsonObject.put("result", "KO");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			jsonObject.put("result", "KO");
		}
		finally {
			try {
				DbUtils.close(rs);
				DbUtils.close(rsKey);
				DbUtils.close(ps);
				DbUtils.close(conn);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		return Response.status(200).entity(jsonObject.toString()).build();
	}
//	END - TCO Project Trakcer
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("util/{utiltype}")
	public Response getUtilType(@PathParam("utiltype")String utilType) throws Exception {

		String selectTable = null;
		JSONObject jsonObject = new JSONObject();
		List<TwoColumnDescrResource> itemList = new ArrayList<>();

		try {
			Connection conn = DBUtil.getSQLiteConnection(DB_NAME);

			if(utilType.equalsIgnoreCase("status")){
				selectTable = "status";
			}
			else if(utilType.equalsIgnoreCase("units")){
				selectTable = "expertise_unit;";
			}
			else if(utilType.equalsIgnoreCase("priority")){
				selectTable = "priority;";
			}
			else if(utilType.equalsIgnoreCase("type")){
				selectTable = "type;";
			}
			else if(utilType.equalsIgnoreCase("subtype")){
				return getSubTypeList();
			}
			else if(utilType.equalsIgnoreCase("projectType")){
				return getProjectTypeList();
			}
			else if(utilType.equalsIgnoreCase("project")){
				return getProjectList();
			}
			else if(utilType.equalsIgnoreCase("projectCode")){
				return getProjectCodeList();
			}
			else if(utilType.equalsIgnoreCase("phase")){
				return getPhaseList();
			}
			else{
				System.err.println(utilType.concat(" missing as web service"));
				throw new Exception();
			}

			conn.setAutoCommit(false);

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT * FROM ".concat(selectTable).concat(" order by 2 desc") );

			while ( rs.next() ) {
				TwoColumnDescrResource resultObj = new TwoColumnDescrResource();

				int id = rs.getInt(1);
				String  descr = rs.getString(2);

				resultObj.setId(id);
				resultObj.setDescr(descr);

				itemList.add(resultObj);
			}

			rs.close();
			stmt.close();
			conn.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		jsonObject.put("util", itemList);

		return Response.status(200).entity(jsonObject.toString()).build();

	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("searchpeople/{text}")
	public String searchPeople(@PathParam("text")String text, @Context HttpHeaders hh) {
		
		Map<String, Cookie> cookies = hh.getCookies();
//		System.out.println("cookies size: " + cookies.size());
		
//		for(String key:cookies.keySet()){
//			Cookie c = cookies.get(key);
//			
//			System.out.println("Cookie name: " + c.getName());
//			System.out.println("Cookie value: " + c.getValue());
//		}
		
		
		Cookie ssoCookie = cookies.get("ObSSOCookie");
		
//		System.out.println("ObSSOCookie Cookie name: " + ssoCookie.getName());
//		System.out.println("ObSSOCookie Cookie value: " + ssoCookie.getValue());
		
		Client client = ClientBuilder.newClient( new ClientConfig().register( ExperimentsResource.class ) );
		WebTarget webTarget = client.target("http://web.global.nibr.novartis.net/services/ps/v1/people/search/"+text);
		  
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder
		                            .cookie(ssoCookie)
		                            .get();
//
//		System.out.println(response.getStatus());
//		System.out.println(response.getStatusInfo());

//		if(response.getStatus() == 200)
//		{
		
//			Employees employees = response.readEntity(Employees.class);
//	        List<Employee> listOfEmployees = employees.getEmployeeList();
//	        System.out.println(Arrays.toString( listOfEmployees.toArray(new Employee[listOfEmployees.size()]) ));
//		}
		
		return response.readEntity(String.class);

	}
	
	
	private Response getProjectTypeList(){
		return getProjectInfo("select descr from type order by 1 desc", new TwoColumnDescrResource());
	}
	
	private Response getProjectList(){
		return getProjectInfo("select distinct name from projects order by 1 desc", new TwoColumnDescrResource());
	}
	
	private Response getPhaseList(){
		return getProjectInfo("select descr from phase order by id asc", new TwoColumnDescrResource());
	}
	
	private Response getProjectCodeList(){
		return getProjectInfo("select distinct code, (name || ' - ' || code) as display_name from projects order by 1 desc", new ThreeColumnDescrResource());
	}
	
	private Response getSubTypeList(){
		JSONObject responseJSON = new JSONObject();
		List<JSONObject> subtypes = new ArrayList<>();
		
		String sql = "select sub_type_id as id, sub_type.descr as descr, type.descr as type from sub_type, type where type.type_id = sub_type.type_id";
		
		Connection conn = DBUtil.getSQLiteConnection(DB_NAME);
		Statement stmt = null;
		ResultSet rs = null;
		
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			
			while(rs.next()){
				JSONObject json = new JSONObject();
				json.put("id", rs.getInt("id"));
				json.put("descr", rs.getString("descr"));
				json.put("type", rs.getString("type"));
				
				subtypes.add(json);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		responseJSON.put("util", subtypes);
		
		return Response.status(200).entity(responseJSON.toString()).build();
	}
	
	private Response getProjectInfo(String sql, Object returnType){
		
		
		JSONObject json = new JSONObject();

		List<Stackable> types = new ArrayList<>();

		Connection conn = DBUtil.getSQLiteConnection(DB_NAME);

		try {
			conn.setAutoCommit(false);


			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			int counter = 0;
			while ( rs.next() ) {
				counter++;
							
				if(returnType.getClass().equals(TwoColumnDescrResource.class)){
					TwoColumnDescrResource item = new TwoColumnDescrResource();
					item.setId(counter);
					item.setDescr( rs.getString(1));
					
					types.add(item);
				}
				else if(returnType.getClass().equals(ThreeColumnDescrResource.class)){
					ThreeColumnDescrResource item = new ThreeColumnDescrResource();
					item.setId(counter);
					item.setDescr( rs.getString(1));
					item.setDescrTwo(rs.getString(2));
					
					types.add(item);
				}
			}

			rs.close();
			stmt.close();
			conn.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		json.put("util", types);
		return Response.status(200).entity(json.toString()).build();
	}
	
		
}
