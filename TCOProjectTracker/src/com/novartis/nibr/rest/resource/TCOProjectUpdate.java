package com.novartis.nibr.rest.resource;

public class TCOProjectUpdate {
	
	private String description;
	private String date;
	private Integer id;
	
	public String getDescription() {
		return description;
	}
	public String getDate() {
		return date;
	}
	public Integer getId() {
		return id;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	

	
	
	
}
