package com.novartis.nibr.rest.resource;

import java.util.List;

public class ProposedExperiment extends TwoColumnDescrResource {
	
	private int priority;
	private int[] expertises;
	private List<String> experts;
	private List<TwoColumnDescrResource> expertiseList;
	private int status;
	private String comment;
	private String priorityStr;
	private String statusStr;
	private String searchedExpert;
	
	public String getSearchedExpert() {
		return searchedExpert;
	}

	public void setSearchedExpert(String searchedExpert) {
		this.searchedExpert = searchedExpert;
	}

	public int[] getExpertises() {
		if(this.expertiseList!=null && this.expertises==null){
			this.expertises = new int[this.expertiseList.size()];
			for(int i=0; i<this.expertiseList.size(); i++){
				this.expertises[i] = this.expertiseList.get(i).getId();
			}
		}
		return this.expertises;
	}

	public List<String> getExperts() {
		return experts;
	}

	public void setExperts(List<String> experts) {
		this.experts = experts;
	}
	public List<TwoColumnDescrResource> getExpertiseList() {
		return expertiseList;
	}
	public void setExpertiseList(List<TwoColumnDescrResource> expertiseList) {
		this.expertiseList = expertiseList;
	}

	public String getPriorityStr() {
		return priorityStr;
	}
	public void setPriorityStr(String priorityStr) {
		this.priorityStr = priorityStr;
	}
	public String getStatusStr() {
		return statusStr;
	}
	public void setStatusStr(String statusStr) {
		this.statusStr = statusStr;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}

}
