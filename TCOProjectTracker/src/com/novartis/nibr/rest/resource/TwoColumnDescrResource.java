package com.novartis.nibr.rest.resource;

public class TwoColumnDescrResource implements Stackable{

	private int id;
	private String descr;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescr() {
		return descr;
	}
	public void setDescr(String descr) {
		this.descr = descr;
	}
	
	
}
