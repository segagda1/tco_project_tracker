package com.novartis.nibr.rest.resource;

public class ThreeColumnDescrResource extends TwoColumnDescrResource implements Stackable{
	
	private String descrTwo;

	public void setDescrTwo(String descrTwo) {
		this.descrTwo = descrTwo;
	}

	public String getDescrTwo() {
		return descrTwo;
	}
	
	

}
