package com.novartis.nibr.rest.resource;

import java.util.List;

public class TCOProject {
	
	private Integer id;
	private String type;
	private String subType;
	private String name;
	private String code;
	private String phase;
	private String priority;
	private String status;
	private String contacts;
	private String description;
	private String sponsor; 
	private Integer trafficlight;
	private String objectives;
	private String deliverables;
	private String timeline;
	private String wgStructure;
	private List<TCOProjectUpdate> projectupdates;
	
	public Integer getId() {
		return id;
	}
	public String getType() {
		return type;
	}
	public String getSubType() {
		return subType;
	}
	public String getName() {
		return name;
	}
	public String getCode() {
		return code;
	}
	public String getPhase() {
		return phase;
	}
	public String getPriority() {
		return priority;
	}
	public String getStatus() {
		return status;
	}
	public String getContacts() {
		return contacts;
	}
	public String getDescription() {
		return description;
	}
	public List<TCOProjectUpdate> getProjectupdates() {
		return projectupdates;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setType(String type) {
		this.type = type;
	}
	public void setSubType(String subType) {
		this.subType = subType;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public void setPhase(String phase) {
		this.phase = phase;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public void setContacts(String contacts) {
		this.contacts = contacts;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setProjectupdates(List<TCOProjectUpdate> projectupdates) {
		this.projectupdates = projectupdates;
	}
	public String getSponsor() {
		return sponsor;
	}
	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}
	public Integer getTrafficlight() {
		return trafficlight;
	}
	public void setTrafficlight(Integer trafficlight) {
		this.trafficlight = trafficlight;
	}
	public String getObjectives() {
		return objectives;
	}
	public String getDeliverables() {
		return deliverables;
	}
	public String getTimeline() {
		return timeline;
	}
	public String getWgStructure() {
		return wgStructure;
	}
	public void setObjectives(String objectives) {
		this.objectives = objectives;
	}
	public void setDeliverables(String deliverables) {
		this.deliverables = deliverables;
	}
	public void setTimeline(String timeline) {
		this.timeline = timeline;
	}
	public void setWgStructure(String wgStructure) {
		this.wgStructure = wgStructure;
	}
	
}
