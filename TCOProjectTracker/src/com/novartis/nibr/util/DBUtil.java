package com.novartis.nibr.util;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBUtil {

	public static Connection getSQLiteConnection(String dbName){
		
		String dbPath = Thread.currentThread().getContextClassLoader().getResource("com/novartis/nibr/util/".concat(dbName)).toString();
		
	//	System.out.println("DB path: " + dbPath);
		
		Connection conn = null;
		
		  try {
		      Class.forName("org.sqlite.JDBC");
		      conn = DriverManager.getConnection("jdbc:sqlite:".concat(dbPath));
		    } catch ( Exception e ) {
		      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		      System.exit(0);
		    }
		    //System.out.println("Opened database successfully");
		    
		return  conn;
	}
	
}
