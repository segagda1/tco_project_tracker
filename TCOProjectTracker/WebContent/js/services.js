var experimentServices = angular.module('experimentServices', ['ngResource']);

experimentServices.factory('TCOProjectServices', ['$resource', function($resource){
	return {
		projects: $resource('/TCOProjectTracker/rest/api/projects/:expId', {}, {
			query: { method: 'GET', params: {}, isArray: false },
			create: {method: 'POST'},
			get: { method: 'GET', params: {expId:'@expId'}, isArray: false },
			update: {method: 'PUT'},
			remove: {method: 'DELETE'}
		}),	
		people: $resource('/TCOProjectTracker/rest/api/searchpeople/:searchText', {}, {
			search: { method: 'GET', params: {searchText:'@searchText'}}
		}),
		util: $resource('/TCOProjectTracker/rest/api/util/:utiltype', {}, {
			//possible utiltype values are: status, units, priority, type, subtype, projectType, project, projectCode, phase
			get: { method: 'GET', params: {utiltype:'@utiltype'}, isArray: false }
		})
	}
}]);


experimentServices.factory('Experiment', ['$resource', function($resource){
	return {
		experiments: $resource('/TCOProjectTracker/rest/api/experiments/:expId', {}, {
			query: { method: 'GET', params: {}, isArray: false },
			create: {method: 'POST'},
			get: { method: 'GET', params: {expId:'@expId'}, isArray: false },
			update: {method: 'PUT'},
			remove: {method: 'DELETE'}
		}),	
		people: $resource('/TCOProjectTracker/rest/api/searchpeople/:searchText', {}, {
			search: { method: 'GET', params: {searchText:'@searchText'}}
		}),
		util: $resource('/TCOProjectTracker/rest/api/util/:utiltype', {}, {
			//possible utiltype values are: experts, status, units, priority, projectType
			get: { method: 'GET', params: {utiltype:'@utiltype'}, isArray: false }
		})
	}
}]);

experimentServices.factory('ExpertHandler',function(){
	
	var factory={};
	
	factory.updateExpertises = function(selExpertise, expertiseList){
		 var newExpertiseList = [];	
		 
		 for(var i=0;i<selExpertise.length;i++){
			 for(var j=0;j<expertiseList.length;j++){
				 if(selExpertise[i]==expertiseList[j].id){
					 newExpertiseList.push(expertiseList[j]);
				 }
			 }
		 }
		 
		 return newExpertiseList;
	}
	
	factory.updateExperts = function(selExperts, expertList){
		var newExpertList = [];
		
		 for(var i=0;i<selExperts.length;i++){
			 for(var j=0;j<expertList.length;j++){
				 if(selExperts[i]==expertList[j].id){
					 newExpertList.push(expertList[j]);
				 }
			 }
		 }
		 
		 return newExpertList;
	}
	
	
	
	return factory;
	
});

experimentServices.factory('UtilFunctions', function(){
	
	var factory = {};
	
	factory.searchPeople = function(searchStr){
//					return Experiment.people.search({searchText:val}, function(data){
//						return data.results.map(function(item){
//							var fullname = item.firstname;
//							return fullname.concat(" ").concat(item.lastname);      
//						});
//					});
					
					return jq.ajax({
						url:'/TCOProjectTracker/rest/api/searchpeople/'+ searchStr,
//						url:'http://web.global.nibr.novartis.net/services/ps/v1/people/search/'+ val,
//						crossdomain: true,
						crossdomain: false,
						xhrFields:  {withCredentials:true}
						}).then(function(data) {
							return data.results.map(function(item){
								var fullname = item.firstname;
								return fullname.concat(" ").concat(item.lastname);      
							});

					 });	
	}
	
	return factory;
});