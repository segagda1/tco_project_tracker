var experimentApp = angular.module('experimentApp', [
                                                     'ngRoute',
                                                     'experimentControllers',
                                                     'experimentFilters',
                                                     'experimentServices',
                                                     'ui.bootstrap',
                                                     'textAngular'
                                                     ]);

experimentApp.config(['$routeProvider', '$httpProvider',
                      function($routeProvider, $httpProvider) {
	$routeProvider.
	when('/experiments/view', {
		templateUrl: 'partials/experiment-list.html',
		controller: 'ExperimentListCtrl'
	}).
	when('/experiments/view/:expId', {
		templateUrl: 'partials/experiment-detail.html',
		controller: 'ExperimentDetailCtrl'
	}).
	when('/experiments/new', {
		templateUrl: 'partials/experiment-create.html',
		controller: 'ExperimentCreateCtrl'
	}).
	otherwise({
		redirectTo: '/experiments/view'
	});
	
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
   

}]);

