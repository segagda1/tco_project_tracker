'use strict';

/* Controllers */


var experimentControllers = angular.module('experimentControllers', []);

experimentControllers.controller('ExperimentListCtrl', ['$scope', '$window', 'Experiment', 'TCOProjectServices', 
      function($scope, $window, Experiment, TCOProjectServices) {
	
		TCOProjectServices.projects.query(function(data){
			$scope.tcoProjectList = data.projects;
		});
	
		Experiment.util.get({utiltype:'status'}, function(data){
			$scope.status = data.util;
		});	
		
		Experiment.util.get({utiltype:'priority'}, function(data){
			$scope.priorities = data.util;
		});
		
		Experiment.util.get({utiltype:'projectType'}, function(data){
			$scope.types = data.util;
		});
		
		Experiment.util.get({utiltype:'project'}, function(data){
			$scope.projectNames = data.util;
		});
		
		Experiment.util.get({utiltype:'projectCode'}, function(data){
			$scope.projectCodes = data.util;
		});
		
		Experiment.util.get({utiltype:'phase'}, function(data){
			$scope.phases = data.util;
		});
		
	 	$scope.goTo = function(url){
	 		$window.location.href = url;
	 	}
	 	
	 	$scope.goEdit = function(index){
	 		$window.location.href = '/TCOProjectTracker/#/experiments/view/'+index;
	 	}
	 		
//		Experiment.util.experts(function(data){
//			$scope.experts = data.experts;
//		});
		
		$scope.orderProp = 'id';
		
		$scope.excelExport = function(JSONData, ReportTitle, ShowLabel) {
			 //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
		    var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
		    
		    var CSV = '';    
		    //Set Report title in first row or line
		    
		    //CSV += ReportTitle + '\r\n\n';

		    //This condition will generate the Label/Header
		    if (ShowLabel) {
		        var row = "";
		        
		        //This loop will extract the label from 1st index of on array
		        /*for (var index in arrData[0]) {
		            
		            //Now convert each value to string and comma-seprated
		            row += index + ',';
		        } */

		        row += 'Project Name' + ',';
		        row += 'Project Code' + ',';
		        row += 'Project Type' + ',';
		        row += 'Project Sub Type' + ',';
		        row += 'Project Phase' + ',';
		        row += 'Priority' + ',';
		        row += 'Status' + ',';
		        row += 'Description' + ',';
		        row += 'Contacts' + ',';
		        row += 'Update Date' + ',';
		        row += 'Update Description' + ',';
		        
		        row = row.slice(0, -1);
		        
		        //append Label row with line break
		        CSV += row + '\r\n';
		    }
		    
		    //1st loop is to extract each row
		    for (var i = 0; i < arrData.length; i++) {
		        var row = "";
		        
		        //2nd loop will extract each column and convert it in string comma-seprated
		      /*  for (var index in arrData[i]) {
		            row += '"' + arrData[i][index] + '",';
		        }*/
		        
		        row += '"' + arrData[i].name + '",';
		        row += '"' + arrData[i].code + '",';
		        row += '"' + arrData[i].type + '",';
		        row += '"' + arrData[i].subType + '",';
		        row += '"' + arrData[i].phase + '",';
		        row += '"' + arrData[i].priority + '",';
		        row += '"' + arrData[i].status + '",';
		        row += '"' + arrData[i].description + '",';
		        row += '"' + arrData[i].contacts + '",';
		        row += '"' + $scope.formatDate(arrData[i].projectUpdateDate) + '",';
		        row += '"' + arrData[i].projectUpdateDescr + '",';

		        row.slice(0, row.length - 1);
		        
		        //add a line break after each row
		        CSV += row + '\r\n';
		    }

		    if (CSV == '') {        
		        alert("Invalid data");
		        return;
		    }   
		    
		    //Generate a file name
		    var fileName = "TCOProjectTracker_";
		    var d = new Date();
		    //this will remove the blank-spaces from the title and replace it with an underscore
		    //fileName += ReportTitle.replace(/ /g,"_");
		    fileName += d.toISOString();
		    
		    //Initialize file format you want csv or xls
		    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
		    
		    // Now the little tricky part.
		    // you can use either>> window.open(uri);
		    // but this will not work in some browsers
		    // or you will not get the correct file extension    
		    
		    //this trick will generate a temp <a /> tag
		    var link = document.createElement("a");    
		    link.href = uri;
		    
		    //set the visibility hidden so it will not effect on your web-layout
		    link.style = "visibility:hidden";
		    link.download = fileName + ".csv";
		    
		    //this part will append the anchor tag and remove it after automatic click
		    document.body.appendChild(link);
		    link.click();
		    document.body.removeChild(link);
			  
		  }
		
		$scope.flatJSON = function(JSONobj){
			var flatJSON = new Array();
			
			for(var i=0; i<JSONobj.length; i++){
				var proj = JSONobj[i];
			
				for(var j=0; j<proj.projectupdates.length; j++){
					var rowItem = new Object();
					
					rowItem.code = proj.code;
					if(proj.contacts){
						rowItem.contacts = proj.contacts.replace(",",";");
					} 
					if(proj.description){
						rowItem.description = proj.description.replace(/(<([^>]+)>)/ig,"");
					}
					rowItem.id = proj.id;
					rowItem.name = proj.name;
					rowItem.phase = proj.phase;
					rowItem.priority = proj.priority;
					rowItem.status = proj.status;
					rowItem.subType = proj.subType;
					rowItem.type = proj.type;
					
					rowItem.projectUpdateDescr = proj.projectupdates[j].descr.replace(/(<([^>]+)>)/ig,"");
					rowItem.projectUpdateDate = new Date(proj.projectupdates[j].date);
					
					flatJSON.push(rowItem);
				}
			}
			return flatJSON;
		}
		

		$scope.formatDate = function formatDate(date) {
		  var monthNames = [
		    "January", "February", "March",
		    "April", "May", "June", "July",
		    "August", "September", "October",
		    "November", "December"
		  ];

		  var day = date.getDate();
		  var monthIndex = date.getMonth();
		  var year = date.getFullYear();

		  return day + ' ' + monthNames[monthIndex] + ' ' + year;
		};
}]);


experimentControllers.controller('ExperimentDetailCtrl', ['$scope', '$routeParams', '$window','Experiment', 'ExpertHandler', 'UtilFunctions', 'TCOProjectServices',
    function($scope, $routeParams, $window, Experiment, ExpertHandler, UtilFunctions,TCOProjectServices) {

	TCOProjectServices.projects.get({expId: $routeParams.expId}, function(data){
			$scope.proj = data.projects[0];
			
			if($scope.proj.projectupdates == null){
				$scope.proj.projectupdates = [];
				var ksq = new Object();
				 $scope.proj.projectupdates.push(ksq);
			}
			
			for(var i=0; i<$scope.proj.projectupdates.length; i++){
				$scope.proj.projectupdates[i].date = new Date($scope.proj.projectupdates[i].date);
			}
			
			//contacts management
			  if($scope.proj.contacts != null){
				  $scope.contactsList = $scope.proj.contacts.split(",");
			  }
			  else{
				  $scope.contactsList = [];
			  }
		});
				
		Experiment.util.get({utiltype:'status'}, function(data){
			$scope.status = data.util;
		});
		
		Experiment.util.get({utiltype:'type'}, function(data){
			$scope.types = data.util;
		});
		
		Experiment.util.get({utiltype:'subtype'}, function(data){
			$scope.subtypes = data.util;
		});
		
		Experiment.util.get({utiltype:'phase'}, function(data){
			$scope.phases = data.util;
		});
		
		Experiment.util.get({utiltype:'priority'}, function(data){
			$scope.priorities = data.util;
		});
		
		 $scope.goTo = function(url){
			 $window.location.href = url;
		 }
		 
		 $scope.deleteKSQ = function(){
			 $scope.proj.projectupdates.splice($scope.selectedKSQ, 1);
		 }
		 
		 $scope.selectKSQ = function(KSQindex){
			 $scope.selectedKSQ = KSQindex;
		 }
		 
		 $scope.selectPE = function(KSQIndex,projIndex){
			$scope.selectedPE = projIndex
			$scope.selectedKSQ = KSQIndex;
		 }
		 
		 $scope.deletePE = function(){
			 $scope.exp.ksqList[$scope.selectedKSQ].projects.splice($scope.selectedPE, 1);
		 }
		
		 $scope.addKSQ = function(){
			 var ksq = new Object();
			 var lastKSQindex = $scope.proj.projectupdates.length-1;
			 
			 $scope.proj.projectupdates.push(ksq);

			 //go to the last project listed before the new ksq
			 if(lastKSQindex>=0){
				 jq('html, body').animate({
					    scrollTop: jq("#fup"+lastKSQindex).offset().top
					}, 1000);
			 }
			 

		 }
		 
		 $scope.addPE = function(ksqIndex){
			 var pe = new Object();
			 if ($scope.exp.ksqList[ksqIndex].projects == null){
				 $scope.exp.ksqList[ksqIndex].projects = new Array();
			 }
			 
			 $scope.exp.ksqList[ksqIndex].projects.push(pe);
		 }
		 
		 $scope.updateExperiment = function(homeRedirect){
			 if($scope.proj.type != "Document" && $scope.proj.type != "QA/QC"){
				 $scope.proj.subType = null;
			 }
			 
			 if($scope.proj.name != "" && $scope.proj.code != ""){
					$scope.mndAlert = null;
					
			 TCOProjectServices.projects.update({expId: $routeParams.expId}, $scope.proj, function(data){
				 if(homeRedirect){
					 $window.location.href = "/TCOProjectTracker/#/experiments/view";
				 }
				 else{
					 //to be changed in a modal dialog
					 alert("Project "+$scope.proj.name+" successfully saved");
				 }
			 });
			 }
			 else{
					$scope.mndAlert = "Please enter mandatory fields";
				}
		 }
		 
		 $scope.deleteProject = function(homeRedirect){
			 TCOProjectServices.projects.remove({expId: $routeParams.expId}, function(data){
					 if(homeRedirect){
						 $window.location.href = "/TCOProjectTracker/#/experiments/view";
					 }
				 });
		    }
		 
//		 $scope.updateExperts = function(ksqIndex,projIndex){
//			 var selExperts = $scope.exp.ksqList[ksqIndex].projects[projIndex].experts;
//			 
//			 $scope.exp.ksqList[ksqIndex].projects[projIndex].expertList = 
//				 ExpertHandler.updateExperts(selExperts, $scope.experts);
//			 
//		 }
		 
		 $scope.updateExpertise = function(ksqIndex,projIndex){
			 var selExpertise = $scope.exp.ksqList[ksqIndex].projects[projIndex].expertises;
			 
			 $scope.exp.ksqList[ksqIndex].projects[projIndex].expertiseList = 
				 ExpertHandler.updateExpertises(selExpertise, $scope.units);
		 }
		 
		  $scope.addContact = function(){
			  if($scope.contactSelected != null && $scope.contactSelected.length>0){
				  if($scope.exp.contacts != null && $scope.exp.contacts.length>0){
					  $scope.exp.contacts += ", ".concat($scope.contactSelected);
				  }
				  else{
					  $scope.exp.contacts = $scope.contactSelected;
				  }
				  
				  $scope.contactsList.push($scope.contactSelected);
				  
				  $scope.contactSelected="";
			  }

		  }
		  
		  $scope.deleteContact = function(index){
			  $scope.contactsList.splice(index, 1);
			  $scope.exp.contacts = "";
			  for(var i=0;i<$scope.contactsList.length;i++){
				  if(i==($scope.contactsList.length-1)){
					  $scope.exp.contacts += $scope.contactsList[i]
				  }
				  else{
					  $scope.exp.contacts += $scope.contactsList[i].concat(",");
				  }
			  }
			  
			 
		  }
		  
		  $scope.searchPeople =  function(val) {
			  if(val.length>3){
				  return UtilFunctions.searchPeople(val);	
			  }
		  };
		  
		  $scope.addExpert = function(ksqIndex,projIndex){
			  var project = $scope.exp.ksqList[ksqIndex].projects[projIndex];
			  var exp = $scope.exp.ksqList[ksqIndex].projects[projIndex].searchedExpert;
			  
			  if(project.experts != null &&
					  project.experts.length > 0){
				  project.experts.push(exp);
			  }
			  else{
				  project.experts = [];
				  project.experts.push(exp);
			  }
			  
			  $scope.exp.ksqList[ksqIndex].projects[projIndex].searchedExpert = "";
			  
		  }
		  
		  $scope.deleteExpert = function(ksqIndex,projIndex,index){
			  $scope.exp.ksqList[ksqIndex].projects[projIndex].experts.splice(index, 1);
		  }
		  
}]);

experimentControllers.controller('ExperimentCreateCtrl', ['$http','$scope','$window', 'Experiment', 'ExpertHandler', 'UtilFunctions', 'TCOProjectServices',
    function($http, $scope, $window, Experiment, ExpertHandler, UtilFunctions, TCOProjectServices){
	
	$scope.ksq_background_colors = ["#F5FAFA", "#C1DAD6", "#ACD1E9", "#6D929B"];
	
	
	$scope.proj = new Object();
	$scope.proj.projectupdates = [];
	$scope.people = "";
	$scope.proj.trafficlight = 0;
	
	 $scope.addKSQ = function(){
		 var ksq = new Object();
		 $scope.proj.projectupdates.push(ksq);
	 }
	 
	 $scope.addPE = function(ksqIndex){
		 var pe = new Object();
		 if ($scope.exp.ksqList[ksqIndex].projects == null){
			 $scope.exp.ksqList[ksqIndex].projects = new Array();
		 }
		 
		 $scope.exp.ksqList[ksqIndex].projects.push(pe);
	 }
	 
	 //init the Project with 1 ksq and 1 proposed experiment
	 $scope.addKSQ();
	 
	 $scope.deleteKSQ = function(){
		 $scope.proj.projectupdates.splice($scope.selectedKSQ, 1);
	 }
	 
	 $scope.selectKSQ = function(fupIndex){
		 $scope.selectedKSQ = fupIndex;
	 }
	 
	 $scope.selectPE = function(KSQIndex,projIndex){
		$scope.selectedPE = projIndex
		$scope.selectedKSQ = KSQIndex;
	 }
	 
	 $scope.deletePE = function(){
		 $scope.exp.ksqList[$scope.selectedKSQ].projects.splice($scope.selectedPE, 1);
	 }
	
	$scope.createExperiment = function(homeRedirect){
		if($scope.proj.type != "Document" && $scope.proj.type != "QA/QC"){
			 $scope.proj.subType = null;
		 }
		
		//check mandatory fields
		if($scope.proj.name != null && $scope.proj.code != null){
			$scope.mndAlert = null;
			
			TCOProjectServices.projects.create($scope.proj,  function(data){
				if(homeRedirect){
					$window.location.href = "/TCOProjectTracker/#/experiments/view";
				}
				else{
					//to be changed in a modal dialog
					alert("Saved!");
				}
			});
		}
		else{
			$scope.mndAlert = "Please enter mandatory fields";
		}
	}
	
	Experiment.util.get({utiltype:'status'}, function(data){
		$scope.status = data.util;
	});
	
	Experiment.util.get({utiltype:'priority'}, function(data){
		$scope.priorities = data.util;
	});
	
	Experiment.util.get({utiltype:'type'}, function(data){
		$scope.types = data.util;
	});
	
	Experiment.util.get({utiltype:'subtype'}, function(data){
		$scope.subtypes = data.util;
	});
	
	Experiment.util.get({utiltype:'phase'}, function(data){
		$scope.phases = data.util;
	});
	
	$scope.clearKsqForm = function(){
		$scope.exp.ksqList.projects = [];
		$scope.exp.ksqList = [];
	}
	
	 $scope.goTo = function(url){
		 $window.location.href = url;
	 }
	 
	 $scope.onEditChangeResult = "";
	 
	 $scope.updateExperts = function(ksqIndex,projIndex){
		 var selExperts = $scope.exp.ksqList[ksqIndex].projects[projIndex].experts;
		 
		 $scope.exp.ksqList[ksqIndex].projects[projIndex].expertList = 
			 ExpertHandler.updateExperts(selExperts, $scope.experts);
		 
	 }
	 
	 $scope.updateExpertise = function(ksqIndex,projIndex){
		 var selExpertise = $scope.exp.ksqList[ksqIndex].projects[projIndex].expertises;
		 
		 $scope.exp.ksqList[ksqIndex].projects[projIndex].expertiseList = 
			 ExpertHandler.updateExpertises(selExpertise, $scope.units);
	 }
	 
	  $scope.searchPeople =  function(val) {
		  if(val.length>3){
			  return UtilFunctions.searchPeople(val);	
		  }
	  };


	  if($scope.proj.contacts != null){
		  $scope.contactsList = $scope.proj.contacts.split(",");
	  }
	  else{
		  $scope.contactsList = [];
	  }
	  
	  $scope.addContact = function(){
		  if($scope.contactSelected != null && $scope.contactSelected.length>0){
			  if($scope.exp.contacts != null && $scope.exp.contacts.length>0){
				  $scope.exp.contacts += ", ".concat($scope.contactSelected);
			  }
			  else{
				  $scope.exp.contacts = $scope.contactSelected;
			  }
			  
			  $scope.contactsList.push($scope.contactSelected);
			  
			  $scope.contactSelected="";
		  }
		  
	  }
	  
	  $scope.deleteContact = function(index){
		  $scope.contactsList.splice(index, 1);
		  $scope.exp.contacts = "";
		  for(var i=0;i<$scope.contactsList.length;i++){
			  if(i==($scope.contactsList.length-1)){
				  $scope.exp.contacts += $scope.contactsList[i]
			  }
			  else{
				  $scope.exp.contacts += $scope.contactsList[i].concat(",");
			  }
		  }
	  }
	  
	  $scope.addExpert = function(ksqIndex,projIndex){
		  var project = $scope.exp.ksqList[ksqIndex].projects[projIndex];
		  var exp = $scope.exp.ksqList[ksqIndex].projects[projIndex].searchedExpert;
		  
		  if(project.experts != null &&
				  project.experts.length > 0){
			  project.experts.push(exp);
		  }
		  else{
			  project.experts = [];
			  project.experts.push(exp);
		  }
		  
		  $scope.exp.ksqList[ksqIndex].projects[projIndex].searchedExpert = "";
		  
	  }
	  
	  $scope.deleteExpert = function(ksqIndex,projIndex,index){
		  $scope.exp.ksqList[ksqIndex].projects[projIndex].experts.splice(index, 1);
	  }
	  

}]);